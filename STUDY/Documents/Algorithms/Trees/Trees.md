# Trees Data Structures

## Introduction
**Binary Tree:** 
    A Tree whose elements can have at a maximum of 2 children nodes.
**Binary Search Tree:**
    A Binary Tree in which a root node has value greater than that of it's left node while lesser than that of it's right node.


**[Representing a Binary Tree]**
```cpp
template <typename T>
struct Node
{
    T value;
    Node* left;
    Node* right;
};
```
+ Any trivial Tree implementation should at least provide functionalities like:
    - Insertion
    - Deletion  (if required)
    - Traversal

#### Properties of Binary Trees
* * * 

- **Maximum Number of Nodes** at any level **i** is **2<sup>i-1</sup>**.
    + Level of Root is 1 (normally, we assume).
- **Maximum Number of Nodes** in a Binary Tree of **height h** is **2<sup>h</sup> - 1**.
- In a Binary Tree with **n** nodes, **Minimum Possible Number of Levels** or **Maximum Possible Height** = **log(n+1)** 

#### Types of Trees
* * *

- **Balanced Binary Tree:**
    A Binary Tree is balanced if the height of Binary Tree is **log(n)**, where **n** is the number of nodes
    + AVL Trees maintain **log(n)** height by making sure that the difference b/w left and right subtrees is 1.
    + Red-Black trees maintain **log(n)** height by making sure that the number of **black** nodes on every **root to leaf path** are same and there are **no adjacent red nodes**.

- **Degenerate / Pathological Tree**
    A Tree where each Internal Node has **only one child**.

_More_@ [Geeks For Geeks](https://www.geeksforgeeks.org/binary-tree-set-3-types-of-binary-tree/)

#### Binary Tree Algorithms
* * *

**[Finding Height of a Binary Tree]**
- **Height** of a Binary Tree is **Maximum Length of Path from Root Node to a Leaf Node**
- One workaround to calculate it, is to add some extra information in the **insert()** method for **Insertion of a New Node**.

```cpp

int height = 0;

//.....

int insert(int value)
{
    // Everytime, we will iterate over an edge or create an edge, we increment our count
    int count = 0;
    if (root == nullptr) {
        root = new Node(value);
        count += 1;         // A new Node is being created, only root exists
    }
    else {
        Node* ptr = root;
        Node* save = nullptr;

        while(ptr != nullptr) {
            if (value > ptr -> value) {
                ptr = ptr -> right;
                count += 1;                 // Iterating over an edge
            }
            else {
                ptr = ptr -> left;
                count += 1;                 // Iterating over an edge
            }
            if (value > save -> value)
                save -> right = new Node(value);
            else
                save -> left = new Node(value);
            count += 1;                     // Adding a new Node
        }
    }
    return count;
}

// .....

for (auto x: input_array)
    height = max(height, tree.insert(x))

```

#### Resources
- **Normal and Binary Trees:** [Tree Archives - Geek for Geeks](http://www.geeksforgeeks.org/category/tree/)
- **Binary Search Tree:** [TopCoder Tutorial - BST and Red-Black Trees](https://www.topcoder.com/community/competitive-programming/tutorials/an-introduction-to-binary-search-and-red-black-trees/)
- **Trees + DP:** [Codeforces - DP on Trees Tutorial](http://codeforces.com/blog/entry/20935)


