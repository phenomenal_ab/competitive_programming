# Presenting the Input/Output to/from the C++ Programs

## Output Formatting

#### Integer Output Manipulations
* * *

##### Outputting Different Number Systems
* * * 
**[Code]**
```cpp
cout << 1234 << " (Decimal)\t"
     << oct << 1234 << " (Octal)\t"
     << hex << 1234 << " (Hexadecimal)";
```
**[Result]**
`1234 (Decimal)     2322 (Octal)    4d2 (Hexadecimal)`

Also, 
**[Code]**
```cpp
cout << 1234 << "\t" << hex << 1234 << "\t" << oct << 1234 << endl;
cout << showbase << dec;
cout << 1234 << "\t" << hex << 1234 << "\t" << oct << 1234 << endl;
```
**[Result]**
```
[Output]:
1234 4d2 2322
1234 0x4d2 02322

[Explanation]:
This happened because 
    Decimal Numbers have no prefix,
    Octal Numbers have prefix 0, 
    Hexadecimal Numbers have prefix 0x

And, this is the exact notation for Integer Literals in C++ Source Code
```

* All the manipulators **oct**, **hex**, **dec**, **showbase** persists
* To cancel the effect of **showbase**, we use the manipulator **noshowbase**

##### Inputting Different Number Systems
* * * 

**[Code]**
```cpp
int a, b, c;
cin >> a >> oct >> b >> hex >> c;
cout << a << "\t" << b << "\t" << c;
```
**[Result]**
```
[Input]:
    1234 2322 4d2
[Output]:
    1234 1234 1234
```

+ We can also make >> accept 0 and 0x for OCTAL and HEXADECIMAL respectively.
+ To do this, we need to unset some defaults,
```cpp
// The stream member function unset() clears the flag passed as Argument
cin.unset(ios::dec);    // Don't assume decimal (so, that 0x could mean HEX)
cin.unset(ios::oct);    // Don't assume octal   (so, that 12 can mean twelve)
cin.unset(ios::hex);    // Don't assume hexadecimal (so, that 12 can mean twelve)
```

**After, unsetting the flags, [Result] (_previous program_)**
```
[Input]:
    1234 02322 0x4d2
[Output]:
    1234 1234 1234
```


### Floating Point Formatting
* * *

>>>
 We use the manipulators **fixed**, **scientific** and **defaultfloat** for
displaying the **Floating Point Number**.
>>>

**[Code]**
```cpp
cout << 1234.56789 << " \t(defaultfloat)" << endl
     << fixed << 1234.56789 << " \t(fixed)" << endl
     << scientific << 1234.56789 << " \t(scientific)" << endl;
```

**[Result]**
```
[Output]:
    1234.57     (defaultfloat)
    1234.567890     (fixed)
    1.234568e+003   (scientific)
```

#### Floating Point Precision
>>>
By default, a floating point value is printed using **6 digits** using the **defaultformat** format.

    1234.567 prints as 1234.57
    1.2345678 prints as 1.23457
    But, 1234567.0 prints as 1.23457e+006
>>>


**Note:**
It happend because the **defaultformat** chooses between the scientific and fixed formats to present the user
with most accurate representation within **6 digits**.

