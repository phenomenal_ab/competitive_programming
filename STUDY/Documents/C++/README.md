# C++

### Constants (**constexpr** or **const** )
+ We use **constexpr** (constant expressions) to signify NUMERIC Constants that are known to us at compile-time.
+ We use **const** (simply, constants) to CONSTANT Values that are calculated at Run-Time.

+ e.g. 
```cpp
constexpr int a = 5;   // Value 5 will be assigned to a @ compile-time
int b;
constexpr int x = b;   // is not correct as 'b' is not a constant-expression

constexpr string s = "hello";   // [ Error ], only Numeric Constants are allowed
const string s = "Hello";       // [ Correct ], any data type is allowed
```
[I/O Manipulation Doc](./docs/IO_Manipulation.md)
